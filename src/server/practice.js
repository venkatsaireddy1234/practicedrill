function restpercountry(a,b){
    let obj ={};
    for (let i in a){
        let count = 0;
        for ( let j in b){
            if(a[i].CountryCode == b[j].CountryCode){
            count++
            }      
        }
        obj[a[i].Country] = count 
    }  
    return obj;   
}


//{country:{city:resno}}

function respcpc(a,b){
    let obj ={}
    for (let i in b){
        for (let j in a){
            if (b[i].CountryCode == a[j].CountryCode){
                if (a[j].Country in obj){
                    if(obj[a[j].Country][b[i].City]){
                        obj[a[j].Country][b[i].City]+=1
                    }else{
                        obj[a[j].Country][b[i].City] =1
                    }
                }else {
                    obj[a[j].Country] = {[b[i].City]:1}
                }
            }
        }
    }
    return obj;
}

//{city:no}

function pizzapercity(b){
    let obj ={};
    for( let i of b){
        let Countryobj = i["Cuisines"]
        if ((typeof Countryobj =="string") && (i.Cuisines.includes("Pizza"))){

            // console.log(i.Cuisines)
            if (obj[i.City]){
                obj[i.City]+=1
            }else{
                obj[i.City]=1
            }
        }
    }
    console.log(obj);
    return obj
}









function expensiveres(a,b,Country){
    let obj ={};
    for (let i in a){
        for (let j in b){
            if (a[i].Country === Country){
                let code = a[i].CountryCode
                if (b[j].CountryCode === code){
                    obj[b[j].City] = {[b[j].RestaurantName]:b[j].AverageCostfortwo,...obj[b[j].City]}
                }
            }
        }
    }
    let newObj = {}
    for(let key in obj){

        newObj[key] = (Object.entries(obj[key]).sort((a,b) => b[1]-a[1])).slice(0,1)

    }
    return newObj;   
}


function highestrated(a,b,Country){
    let obj ={};
    for (let i in a){
        for (let j in b){
            if (a[i].Country === Country){
                let code = a[i].CountryCode
                if (b[j].CountryCode === code){
                    obj[b[j].City] = {[b[j].RestaurantName]:b[j].Aggregaterating,...obj[b[j].City]}
                }
            }
        }
    }


    let newObj = {}
    for(let key in obj){

        newObj[key] = (Object.entries(obj[key]).sort((a,b) => b[1]-a[1])).slice(0,1)

    }

    return newObj;
}

//{cuisine:res};
function respercuisine(b){
    let obj = {};
    for (i of b){
        let Countryobj = i["Cuisines"].toString().split('-')
        for (let i in Countryobj){
            if (obj[Countryobj[i]]){
                obj[Countryobj[i]]+=1
            }else{
                obj[Countryobj[i]]=1
            }
        }
    }
    return obj
}
















module.exports.respcpc=respcpc;
module.exports.restpercountry=restpercountry;
module.exports.pizzapercity=pizzapercity;
module.exports.expensiveres=expensiveres;
module.exports.highestrated=highestrated;
module.exports.respercuisine=respercuisine;




//HOF
module.exports.respercuisineHOF=respercuisineHOF;;
module.exports.pizzapercityHOF=pizzapercityHOF;
module.exports.restaurantspcpcityHOF=restaurantspcpcityHOF;
module.exports.mostexpensiverespcityHOF=mostexpensiverespcityHOF;
module.exports.restuarantsPerCountryHOF=restuarantsPerCountryHOF;
module.exports.highratedHOF=highratedHOF;

function restaurantspcpcityHOF(a,b){
    const acc = {}
    const output = b.map((elem)=>{
        const Countryobj = a.find((ele)=> ele.CountryCode == elem.CountryCode);
        if(Countryobj){
            if (acc.hasOwnProperty(Countryobj.Country)){
                if (acc[Countryobj.Country][elem.City]){
                    acc[Countryobj.Country][elem.City]+=1
                }else{
                    acc[Countryobj.Country][elem.City] =1
                }
            }else{
                acc[Countryobj.Country]={[elem.City]:1}
            }
        }
    })
   
    return acc
}

function restuarantsPerCountryHOF(a,b){
    let result ={};
    let array = b.map((elem)=>{
        let obj = a.find(ele =>ele.CountryCode === elem.CountryCode)
        if(obj){
            if(result[obj.Country]){
                result[obj.Country]+=1
            }else{
                result[obj.Country] =1
            }
        }
    })
    return result;
}



function mostexpensiverespcityHOF(a,b,Country){
    let result = b.reduce((acc,current)=>{
        let Countryobj = a.find(ele => ele.Country == Country)
        
        if (Countryobj.CountryCode === current.CountryCode){
            acc[current.City] = {[current.RestaurantName]:current.AverageCostfortwo,...acc[current.City]}
        }
        return acc
       
    },{})
    let resobj = {};
    let Countryobjay = Object.keys(result)
    Countryobjay.map((ele)=>{
        let rest=result[ele]
        resobj[ele] = Object.keys(rest).sort((a,b) => rest[b]-rest[a]).slice(0,1)
    })
    return resobj
}


function highratedHOF(a,b,Country){
    let result = b.reduce((acc,current)=>{
        let Countryobj = a.find(ele => ele.Country == Country)
        
        if (Countryobj.CountryCode === current.CountryCode){
            acc[current.City] = {[current.RestaurantName]:current.Aggregaterating,...acc[current.City]}
        }
        return acc
       
    },{})
    let high ={};
    let cities=Object.keys(result);
    cities.map((ele)=>{
        let restarnts = result[ele]
        high[ele] = Object.keys(restarnts).sort((a,b) => restarnts[b]-restarnts[a]).slice(0,1)
    })
    return high;
}















function pizzapercityHOF(b){
    let output = b.reduce((acc,current)=>{
        let splitted = current["Cuisines"]
        let yes = splitted.includes("Pizza")
        if (yes){
            if(acc[current["City"]]){
                acc[current["City"]] +=1
            }else{
                acc[current["City"]] =1
            }
        }
    return acc
        
    },{})   
   return output
}


function respercuisineHOF(b){
    let Countryobj = b.map((ele) => {
        return ele["Cuisines"].toString().split('-')
    })
    let res = {};
    Countryobj.reduce((acc,current)=>{
        let cuisine = current.map((ele)=>{
            if(res[ele]){
                res[ele]+=1
            }else{
                res[ele]=1
            }
        })
    })
    return res
}

